"""An implementation of the Relative Upper Confidence Bound (RCB) algorithm."""
from typing import Callable
from typing import List
from typing import Optional
from typing import Set
from typing import Tuple

import numpy as np


def _random_argmax_excluding(
    array: np.array,
    exclude_idx: int,
    random: np.random.RandomState = np.random.RandomState(),
) -> int:
    """Find an index with maximum value, excluding ``exclude_idx``.

    It removes the exclude_idx upper bound value from the column array and calculates the indices with maximum value.
    If more than one item with the maximal value exists, it chooses one of them randomly.

    Parameters
    ----------
    array
        Selected arm column of upper confidence bound array.
    exclude_idx
        A selected arm of upper confidence bound array.
    random
        A NumPy random state. Defaults to an unseeded state when not specified.

    Returns
    -------
    int
        A randomly chosen element from the set of elements with maximal value.
    """
    max_value = np.amax(np.delete(array, exclude_idx))
    indices = set(np.ndarray.flatten(np.argwhere(array == max_value)))
    indices = indices - {exclude_idx}
    return random.choice(list(indices))


class RelativeUCB:
    """Implementation of the relative upper confidence bound algorithm.

    In the algorithm presented in paper [1]_, we assume that there exists a Condorcet winner. This algorithm is
    extension of the Upper Confidence Bound(UCB) algorithm and is motivated as learning from the relative feedback rather
    than real value feedback between two arms. It works for both finite as well as for infinite time horizon. The
    major goals of this algorithm is to minimize cumulative regret overtime for the K armed dueling bandit problem and
    also return a Condorcet winner.

    In each time-step RUCB executes three sub-part sequentially:

    First part: Initially, Assume all arms as a potential champion. All arms are compared in pairwise optimistically
    fashion using upper confidence bound. If the upper confidence bound of an arm against any other arm is less then
    0.5, then that "loser" is removed from the potential champions. This process keeps on and when we are left with
    only one arm in the pool then that arm is assigned as the hypothesized best arm. There is always at most one
    hypothesized best arm. This hypothesized best arm(B) is demoted from its status as soon as it loses to another
    arm and from the remaining potential champions arm, a potential champion arm(arm_c) is chosen in two ways: if B is
    not present,we sample an arm uniformly randomly; if B is present, the probability of picking the arm B is set to 1/2
    and the remaining arms are given equal probability for being chosen.

    Second part: Regular UCB is performed using arm_c(potential champion) as a benchmark. Now, we select challenger arm
    arm_d(other than potential arm -arm_c) whose upper confidence bound is maximum with reference to potential champion
    (arm_c).

    Last part: Now the potential champion and challenger arm (arm_c,arm_d) is compared. Based on the comparison, the
    winner arm is decided and the wining count is updated. At last, Condorcet arm is returned from arm pool whose wining
    count is maximum.

    Attributes
    ----------
    feedback
        Feedback function for comparing two arms.
    exploratory_constant
        The confidence radius grows proportional to the square root of this value.
    rounds
        The number of time steps to run. ``None`` in the case of an infinite time horizon. Assigning real numeric value
        to it is the only way to stop the algorithm.
    num_of_arms
        Total number of arms.

    References
    ----------
    .. [1] Masrour Zoghi, Shimon Whiteson, Remi Munos, and Maarten de Rijke. Relative Upper Condence Bound for the
    K-Armed Dueling Bandit Problem. In Proceedings of International Conference on Machine Learning (ICML),
    pages 10-18, 2014a.
    """

    def __init__(
        self,
        feedback: Callable[[int, int], int],
        exploratory_constant: float,
        num_of_arms: int,
        rounds: Optional[int] = None,
    ) -> None:
        if rounds is not None and rounds <= 0:
            raise ValueError(
                f"rounds must be greater than zero, but provided number of"
                f" rounds is {rounds}."
            )
        if num_of_arms < 2:
            raise ValueError(
                f"Number of arms should be more than one but provided number of arms is {num_of_arms}."
            )
        if exploratory_constant <= 0.5:
            raise ValueError(
                f"Value of exploratory_constant should always be greater 0.5 but provided exploratory constant "
                f"value is {exploratory_constant}."
            )
        self.feedback = feedback
        # Each step in the code refers to lines of RUCB algorithm presented in the paper.
        # Step 1: Initialization of count of wins between each arm as win_matrix
        self.win_matrix = np.zeros((num_of_arms, num_of_arms))
        # Step 2: Initialization hypothesized best arm
        self.hypothesized_arm: Optional[int] = None
        self.exploratory_constant = exploratory_constant
        self.rounds = rounds
        self.num_of_arms = num_of_arms
        self.time_step = 0

    def _compute_upper_confidence_matrix(self) -> np.array:
        """Compute the current upper confidence matrix using Hoeffding bounds.

        Returns
        -------
        np.array
            Upper confidence bound matrix for given time stamp.
        """
        upper_confidence_bound_matrix = np.zeros((self.num_of_arms, self.num_of_arms))
        for arm_i in range(self.num_of_arms):
            for arm_j in range(self.num_of_arms):
                if arm_i == arm_j:
                    #   Step 5:U[i,j] diagonal values change to 0.5
                    upper_confidence_bound_matrix[arm_i][arm_j] = 0.5
                    continue
                total_comparisons = (
                    self.win_matrix[arm_i][arm_j] + self.win_matrix[arm_j][arm_i]
                )
                if total_comparisons == 0:
                    preference_estimate = 1  # Element wise x/0 is 1
                    confidence_radius = 1  # Element wise x/0 is 1
                else:
                    preference_estimate = (
                        self.win_matrix[arm_i][arm_j] / total_comparisons
                    )
                    confidence_radius = np.sqrt(
                        (self.exploratory_constant * np.log(self.time_step))
                        / total_comparisons
                    )
                upper_confidence_bound_matrix[arm_i][arm_j] = (
                    preference_estimate + confidence_radius
                )
        return upper_confidence_bound_matrix

    def _select_potential_winner(self, potential_arms: Set[int]) -> int:
        """Select a potential winner form the set of potential winners.

        Parameters
        ----------
        potential_arms
           Set of potential winner arms.

        Returns
        -------
        int
            Selected arm from the given set of potential winners.
        """
        if len(potential_arms) == 1:
            self.hypothesized_arm = list(potential_arms)[0]
            # arm_c be the unique element of potential arm set
            arm_c = self.hypothesized_arm

        # Step 10: if more than one potential arm
        if len(potential_arms) > 1:
            # probability distribution for list of arms
            probability_distribution = dict()
            if self.hypothesized_arm is not None:
                probability_distribution[self.hypothesized_arm] = 0.5
                potential_arms = potential_arms - {self.hypothesized_arm}

            for potential_arm in potential_arms:
                # distribute probability equal to other arms
                probability_distribution[potential_arm] = 1 / (
                    np.power(2, 0 if self.hypothesized_arm is None else 1)
                    * len(potential_arms - {self.hypothesized_arm})
                )
            #   Step 11:sample arm_c from probability distribution
            arm_c = np.random.choice(
                list(probability_distribution.keys()),
                p=list(probability_distribution.values()),
            )
        return arm_c

    def _get_champion(self) -> int:
        """Get the champion arm that have won more number of times than any other arm.

        Returns
        -------
        int
            Champion arm at time-step T.
        """
        arms_win_count = np.zeros(self.num_of_arms)

        # calculate number of expected wins for each arm
        for arm_i in range(self.num_of_arms):
            for arm_j in range(self.num_of_arms):
                total_comparision = (
                    self.win_matrix[arm_i][arm_j] + self.win_matrix[arm_j][arm_i]
                )
                if arm_i == arm_j or total_comparision == 0:
                    continue
                preference_estimate = self.win_matrix[arm_i][arm_j] / total_comparision
                if np.float(preference_estimate) > 0.5:
                    arms_win_count[arm_i] += 1

        winner_arm = int(np.argmax(arms_win_count))
        return winner_arm

    def step(self) -> None:
        """Run one round of an algorithm."""
        self.time_step += 1
        # Step 4: computation for upper confidence bound matrix U[i,j]
        #   µ[i,j]
        upper_confidence_bound_matrix = self._compute_upper_confidence_matrix()
        # Step 6: compute potential champion of arms
        potential_arms = set()
        for arm_i in range(self.num_of_arms):
            could_be_winner = True  # selected arm wins all other arm or not
            for arm_j in range(self.num_of_arms):
                if upper_confidence_bound_matrix[arm_i][arm_j] < 0.5:
                    could_be_winner = False
            if could_be_winner:
                potential_arms.add(arm_i)

        # Step 7: potential set is empty
        if len(potential_arms) == 0:
            arm_c = np.random.choice(range(self.num_of_arms))
            potential_arms.add(arm_c)

        # Step 8: hypothesized arm is no longer a potential winner
        if (
            self.hypothesized_arm is not None
            and self.hypothesized_arm not in potential_arms
        ):
            self.hypothesized_arm = None

        # Step 9-12: select an potential champion arm(arm_c) from potential arms
        arm_c = self._select_potential_winner(potential_arms)

        # Step 13: selection of challenger arm(arm_d) where potential champion and challenger should not be same
        # Select the upper confidences of all the arm with respect to potential champion(arm_c)
        upper_confidences = upper_confidence_bound_matrix[:, arm_c]
        # Select challenger arm - arm_d(other than arm_c) whose upper confidence bound is maximum with reference to arm_c.
        arm_d = _random_argmax_excluding(upper_confidences, arm_c)

        # Step 14: Compare potential champion(arm_c) and challenger(arm_d).
        if self.feedback(arm_c, arm_d) < 0:
            winner, loser = arm_d, arm_c
        else:
            winner, loser = arm_c, arm_d

        self.win_matrix[winner][loser] += 1

    def run(self) -> int:
        """Execute the Relative upper confidence bound algorithm.

        Returns
        -------
        int
            Condorcet winner after running the user provided rounds
        """
        # Step 3: Iterate algorithm over finite or infinite number of rounds.
        while self.time_step != self.rounds:
            self.step()
        # Step 15: end of iteration.
        return self._get_champion()


def _main() -> None:
    preference_matrix = np.array(
        [
            [0.5, 0.8, 0.7, 0.6],
            [0.2, 0.5, 0.75, 0.75],
            [0.3, 0.25, 0.5, 0.6],
            [0.4, 0.25, 0.4, 0.5],
        ]
    )
    history_array: List[Tuple[int, int, int]] = []

    # Copy of winner stays algorithm analyze regret
    def calculate_weak_regret(best_arm: int) -> float:
        """Calculate the weak regret for the algorithm.

        The weak regret is the minimal distance between the chosen arms and the best arm.

        Parameters
        ----------
        best_arm
            The best arm among the total number of arms

        Returns
        -------
        regret_history
            A list of history containing the weak regret per round.
        cumulative_regret
            The cumulative weak regret for all the history regret.
        """
        cumulative_regret = 0
        for arm_i, arm_j, _ in history_array:
            weak_regret_round = (
                min(
                    preference_matrix[best_arm, arm_i],
                    preference_matrix[best_arm, arm_j],
                )
                - 0.5
            )
            cumulative_regret += weak_regret_round
        return cumulative_regret

    # copy of winner stays algorithm test feedback
    def test_feedback(i: int, j: int) -> int:
        """Calculate feedback to test the algorithm.

        The winner is chosen according to the probability in the preference matrix.

        Parameters
        ----------
        i
            The index of the first arm.
        j
            The index of the second arm.

        Returns
        -------
        feedback
            The feedback, either positive if `i` won, or negative if `j` won.
        """
        rand = np.random.uniform()
        winner = i if rand < preference_matrix[i, j] else j
        feedback = 1 if winner == i else -1
        (i, j) = (j, i) if feedback is -1 else (i, j)
        history_array.append((i, j, feedback))
        # condition to stop the infinite loop user has to provide some constraint
        # loop terminate either assigning total time with some value by user.
        if test_object.time_step == 500:
            test_object.win_matrix[i][j] += 1
            test_object.rounds = test_object.time_step
        return feedback

    print("Output for finite time horizon")
    test_object = RelativeUCB(test_feedback, 0.7, 4, 20)
    print("Best arm:", test_object.run())
    cumul_r = calculate_weak_regret(0)
    print(f"Cumulative weak regret: {cumul_r}")
    history_array = []

    print("Output for infinite time horizon")
    test_object = RelativeUCB(
        test_feedback, 0.6, 4
    )  # for infinite loop do not prove time horizon
    # value zero is a indication of infinite time horizon
    print("Best arm:", test_object.run())
    cumul_r = calculate_weak_regret(0)
    print(f"Cumulative weak regret: {cumul_r}")


if __name__ == "__main__":
    _main()
