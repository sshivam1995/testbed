"""Find the condorcet winner in a Preference Based Multi-arm bandit problem using 'Beat the Mean Bandit' algorithm."""

from typing import Callable

import numpy as np


class BeatTheMeanBandit:  # pylint: disable=too-many-instance-attributes, too-many-arguments
    """Implements the 'Beat the Mean Bandit' algorithm.

    This is an explore-then-exploit algorithm assuming a total order over arms, relaxed stochastic transitivity and the stochastic triangle inequality.
    The Beat the Mean algorithm [1]_ gives the Condorcet winner which is the best arm in the provided set of arms.
    It proceeds in a sequence of rounds, and maintains a working set of active arms during each round.
    For each active arm, an empirical estimate is maintained for how likely an arm is to beat the mean bandit of the working set.
    In each iteration, an arm with the fewest recorded comparisons is selected for comparison.
    The algorithm terminates when only one active arm remains, or when time horizon is reached.

    Parameters
    ----------
    number_of_arms
        The number of arms available to the algorithm. This assumes a fixed, finite number of arms.
    time_horizon
        The total number of rounds.
    compare
        A function taking two arm indices as an input and returning the winning arm.

    Attributes
    ----------
    working_set
        Stores the set of active arms. Corresponds to the W1 in [1]_.
    comparisons
        Stores the total number of comparisons of a specific arm. Corresponds to n_b in [1]_.
    wins
        Stores the number of wins of each arm. Corresponds to w_b in [1]_.
    probability_estimate
        Stores the empirical estimate of arms versus the mean bandit. Corresponds to P_b in [1]_.
    random
        A numpy random state. Defaults to an unseeded state when not specified.

    References
    ----------
    .. [1] Yisong Yue and Thorsten Joachims. "Beat the Mean Bandit." Proceedings of International Conference on Machine Learning (ICML), pages 241-248, 2011.
    """

    def __init__(
        self,
        number_of_arms: int,
        time_horizon: int,
        compare: Callable[[int, int], int],
        random: np.random.RandomState = np.random.RandomState(),
    ) -> None:
        self.number_of_arms = number_of_arms
        self.time_horizon = time_horizon
        self.compare = compare
        self.working_set = np.arange(self.number_of_arms)
        self.comparisons = np.zeros((number_of_arms, number_of_arms), dtype=int)
        self.wins = np.zeros((number_of_arms, number_of_arms), dtype=int)
        self.probability_estimate = np.zeros(number_of_arms, dtype=float)
        for index in self.working_set:
            self.probability_estimate[index] = 0.5
        self.random = random

    def remove_from_working_set(self, worst_arm: int) -> None:
        """Remove the worst arm from the working set and update the comparisons, wins and probability estimate of each arm in the current working set.

        Parameters
        ----------
        worst_arm
            The index of the empirically worst arm.
        """
        self.comparisons[worst_arm, :] = 0
        self.comparisons[:, worst_arm] = 0  # removing the comparisons of the worst_arm
        self.wins[:, worst_arm] = 0
        self.wins[worst_arm, :] = 0  # updating the wins
        for arm in self.working_set:
            if np.sum(self.comparisons[arm, :]) == 0:
                self.probability_estimate[arm] = 0.5
            else:
                self.probability_estimate[arm] = np.sum(self.wins[arm, :]) / np.sum(
                    self.comparisons[arm, :]
                )  # update the probability_estimate
        self.working_set = np.delete(
            self.working_set, np.argwhere(self.working_set == worst_arm)
        )  # update the working set by removing the worst_arm

    def beat_the_mean_bandit(self) -> int:
        """Execute the algorithm.

        Run through each time_step till time_horizon.
        This includes choosing arms, comparing them and updating the estimates.

        Returns
        -------
        int
            The arm with the highest empirical estimate.
        """
        confidence_radius = 1
        time_step = 0
        while (len(self.working_set) > 1) and (time_step < self.time_horizon):
            arm1 = self.random.choice(
                argmin_set(self.comparisons)
            )  # break ties randomly
            arm2 = self.random.choice(
                self.working_set
            )  # select arm2 from the current working_set at random
            if arm1 == arm2:
                continue
            winner = self.compare(arm1, arm2)  # compare both arms
            if winner == arm1:
                self.wins[arm1][arm2] += 1
            self.comparisons[arm1][arm2] += 1
            self.comparisons[arm2][arm1] += 1
            self.probability_estimate[arm1] = np.sum(self.wins[arm1, :]) / np.sum(
                self.comparisons[arm1, :]
            )  # update the unbiased probability whenever comparisons or wins are updated
            time_step += 1
            if (
                min(self.probability_estimate) + confidence_radius
                <= max(self.probability_estimate) - confidence_radius
            ):
                worst_arm = np.argmin(self.probability_estimate).item()
                self.remove_from_working_set(worst_arm)
        return np.argmax(self.probability_estimate).item()


def argmin_set(array: np.array) -> np.array:
    """Calculate the complete argmin set, returning an array with all indices.

    Parameters
    ----------
    array
        The array for which the argmin should be calculated

    Returns
    -------
    indices
        An 1-D array containing the argmin set
    """
    # np.argmin returns the first index, to get the whole set we search for all indices which point to a value equal to the minimum
    min_value = array.min()
    indices = np.argwhere(array == min_value).flatten()
    return indices


def _main() -> None:
    """Test the algorithm implementation."""
    preference_matrix = np.array(
        [
            [0.5, 0.65, 0.65, 0.65],
            [0.35, 0.5, 0.7, 0.7],
            [0.35, 0.3, 0.5, 0.6],
            [0.35, 0.3, 0.4, 0.5],
        ]
    )

    def compare(arm1: int, arm2: int) -> int:
        """Return the winning arm.

        The winner is chosen according to the probability in the preference matrix.

        Parameters
        ----------
        arm1
            The index of the first arm.
        arm2
            The index of the second arm.

        Returns
        -------
        int
            The winning arm index.
        """
        return arm1 if np.random.uniform() < preference_matrix[arm1, arm2] else arm2

    # run the algorithm
    btm = BeatTheMeanBandit(number_of_arms=4, time_horizon=4, compare=compare)
    best_arm = btm.beat_the_mean_bandit()
    print("Winning arm: arm", best_arm)


if __name__ == "__main__":
    _main()
