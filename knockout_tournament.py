"""Implementation of the knockout tournament algorithm."""

from itertools import combinations
import math
from typing import Callable
from typing import Tuple

import numpy as np


class KnockOutTournament:
    """Implementation of the knockout tournament algorithm.

    The goal is to find the Condorcet winner as well as the best ranking in the PAC setting. The algorithm assumes a total order over the
    existing arms and that strong stochastic transitivity, stochastic triangle inequality and relaxed stochastic transitivity hold. It takes
    the set of arms as an input and compares them in rounds. At the end of each round, the size of the input is halved. The winning arm for a round is decided
    using the bias and confidence values.
    The algorithm runs in rounds, where in each round it randomly pairs the arms into group and the winners are proceeded into the next round.
    For example that we have four arms (A, B, C, D). It will first group the arms in pairs like [A, B] as the first pair and [C, D] as the
    second pair. After grouping them in pairs, the algorithm pulls out the winner from each pair, and the winners moves to the next round.

    Parameters
    ----------
    arm_set
    The number of arms available to the algorithm. This assumes a fixed, finite number of arms
    feedback
          The function is used to update information about reactions of the arms.
    bias
            Bias is used to compare the calibrated preference probability of arm_i beating arm_j
    confidence
            is used to determine the number of iterations required to reach to the conclusion in order to gain higher confidence about the chosen arm
    stochasticity
            is used to find the maximum selection and ranking using pairwise comparison.

    References
    ----------
    ... [1] Moein Falahatgar, Alon Orlitsky, Venkatadheeraj Pichapati and Ananda Theertha Suresh. "Maximum Selection and Ranking under Noisy Comparisons".
    pages 1-7, 2017. arxiv.org/pdf/1705.05366
    """

    # pylint: disable=too-many-arguments
    def __init__(
        self,
        feedback: Callable[[float, float], float],
        arm_set: set,
        bias: float,
        confidence: float,
        stochasticity: float,
    ):

        self.feedback = feedback
        self.arm_set = arm_set
        self.bias = bias
        self.confidence = confidence
        self.stochasticity = stochasticity

    def _round(self, arm_set: set) -> set:
        """Arms are paired into groups, and each pairs of arms is send to COMPARE, after which the COMPARE returns the set of winners in each pair.

        Parameters
        ----------
        arm_set
            The set of arms available to the algorithm. This assumes a fixed, finite number of arms.

        Returns
        -------
        set
            it returns the set of winners.
        """
        winning_arms = set()
        # pair element randomly
        pairs = combinations(arm_set, 2)
        for arm_i, arm_j in pairs:
            winning_arms.add(self._compare(arm_i, arm_j))
        return winning_arms  # it discard the inferior arm and return the winning arm in each pair

    def run(self) -> set:
        """Run function iterates over the arms in the arm_set. it calls round function that pairs the arms into group of two and compares them. After which it returns the set of winning arms from each pair."""
        iteration_arm_i = 1
        confidence = math.pow(2, 1 / 3) - 1
        while len(self.arm_set) > 1:
            self.bias = (confidence * self.bias) / (
                math.pow(self.stochasticity, 2) * math.pow(2, iteration_arm_i / 3)
            )
            self.confidence = self.confidence / math.pow(2, iteration_arm_i)
            self.arm_set = self._round(self.arm_set)
            iteration_arm_i += 1
        return self.arm_set

    def _compare(self, arm_i: float, arm_j: float) -> float:
        """Compare the two arms i, j and output the winning arms. The preferred arm will proceed to the next round.

        Parameters
        ----------
        i
            index of the first arm
        j
            index of the second arm

        Returns
        -------
        int
            The index of the winning arm
        """
        estimate_probability_arm_i = 0.5
        estimate_confidence_radius = 0.5
        # In order to gain more confidence about the winning arm in each COMPARE, we repeat each comparison several times. Comparison stop when it reaches the comparison budget, it output the arm with more wins.
        comparison_budget = (1 / (2 * math.pow(self.bias, 2))) * np.log(
            2 / self.confidence
        )
        rounds = 0
        winner_arm_i = 0

        while (
            np.abs(estimate_probability_arm_i - 0.5)
            <= estimate_confidence_radius - self.bias
            and rounds <= comparison_budget
        ):
            # it updates information about the preferred and eliminated arms
            if self.feedback(arm_i, arm_j) < 0:
                winner = arm_j
            else:
                winner = arm_i
            if arm_i is winner:
                winner_arm_i = winner_arm_i + 1
            rounds = rounds + 1
            estimate_probability_arm_i = winner_arm_i / rounds
            estimate_confidence_radius = np.sqrt(
                (1 / (2 * rounds)) * np.log((4 * math.pow(rounds, 2) / self.confidence))
            )

        if estimate_probability_arm_i <= 0.5:
            return arm_j
        return arm_i


if __name__ == "__main__":
    # arm 2 is the condorcet winner, the arms are ranked as 1,2,3,4
    preference_matrix = np.array(
        [
            [0.5, 0.1, 0.2, 0.3],
            [0.9, 0.5, 0.9, 0.9],
            [0.8, 0.1, 0.5, 0.6],
            [0.7, 0.1, 0.4, 0.5],
        ]
    )
    history_ = []

    # Copy of winner stays algorithm analyze regret
    def calculate_weak_regret(
        history: list, preference_matrix_: np.array, best_arm_idx: int
    ) -> Tuple[list, float]:
        """Calculate the weak regret.

        The weak regret is the minimal distance between the chosen arms and the best arm.

        Parameters
        ----------
        history
            The history of chosen arms.

        preference_matrix
            The preference matrix for the given problem.

        best_arm_idx
            The index of the best arm.

        Returns
        -------
        regret_history
            A list containing the weak regret per round.

        cumul_regret
            The cumulative weak regret.
        """
        regret_history = []
        cumul_regret = 0
        for i, j, _ in history:
            weak_regret = (
                min(
                    preference_matrix_[best_arm_idx, i],
                    preference_matrix_[best_arm_idx, j],
                )
                - 0.5
            )
            regret_history.append(weak_regret)
            cumul_regret += weak_regret
        return regret_history, cumul_regret

    # copy of winner stays algorithm test feedback
    def test_feedback(i: float, j: float) -> float:
        """Calculate feedback to test the algorithm.

        The winner is chosen according to the probability in the preference matrix.

        Parameters
        ----------
        i
            The index of the first arm.
        j
            The index of the second arm.

        Returns
        -------
        feedback
            The feedback, either positive if `i` won, or negative if `j` won.
        """
        rand = np.random.uniform()
        winner = i if rand < preference_matrix[i, j] else j
        feedback = 1 if winner == i else -1
        (i, j) = (j, i) if feedback is -1 else (i, j)
        history_.append((i, j, feedback))
        return feedback

    history_ = []
    print("Output for infinite time horizon")
    test_object = KnockOutTournament(
        test_feedback, set(range(len(preference_matrix))), 0.2, 0.4, 0.3
    )  # for infinite loop do not prove time horizon
    # value zero is a indication of infinite time horizon
    test_object.run()
    print("Best arm:", test_object.arm_set)
    _, cumul_r = calculate_weak_regret(history_, preference_matrix, 0)
    print(f"Cumulative weak regret: {cumul_r}")
